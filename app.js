//Estoy importando el paquete colors
require('colors');

const argv = require('yargs').argv;

const { generarArchivo } = require('../Ejercicios Bases/helpers/multiplicar');


//const base = 5;

console.log(process.argv);
console.log(argv);

console.log('base: yargs', argv.base);

// const [, , arg3 = 'base=9'] = process.argv;
// const [, base] = arg3.split('=')
// console.log(base);

generarArchivo(argv.base)
  .then(nombreArchivo => console.log(nombreArchivo.yellow, 'creado'))
  .catch(err => console.log(err));